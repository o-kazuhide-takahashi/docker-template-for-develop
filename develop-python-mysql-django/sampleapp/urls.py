"""
URL configuration for samplesite project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))

python manage.py runserver --settings=samplesite.settings 0.0.0.0:8080

python manage.py createsuperuser
"""

from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("sample", views.sample, name="sample"),
    path("logtest", views.logtest, name="logtest"),
    path("mailtest", views.mailtest, name="mailtest"),

    path("bootstrap5", views.bootstrap5, name="bootstrap5"),
    path("adminlte3", views.adminlte3, name="adminlte3"),

]
