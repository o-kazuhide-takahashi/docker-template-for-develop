"""
pip show django-adminlte-3

Name: django-adminlte-3
Version: 0.1.6
Summary: Admin LTE templates, admin theme, and template tags for Django
Home-page: https://github.com/d-demirci/django-adminlte3
Author: d3n1z
Author-email: d3n1z@protonmail.com
License: MIT
Location: /home/ec2-user/.pyenv/versions/3.12.1/lib/python3.12/site-packages
Requires: django
Required-by:
"""

import json
import logging
import ppretty

from django.core.mail import send_mail
from django.shortcuts import render
# Create your views here.

from django.http import HttpResponse

def index(request):
    logger = logging.getLogger('django')
    logger.info('sample index info')
    return HttpResponse("Hello, world. You're at the sample index.")

def sample(request):
    logger = logging.getLogger('django')
    logger.info('Hello World! info')
    return HttpResponse(
        '' + (json.dumps({"c": 0, "b": 0, "a": 0}, sort_keys=True, indent=4))
    )

def logtest(request):
    logger = logging.getLogger('django')
    # logger.debug('Hello World!')
    logger.info('Hello World! info')
    # logger.warning('Hello World! warning')
    # logger.error('Hello World! error')
    return HttpResponse(
        ppretty.ppretty(request)
    )

def mailtest(request):
    try:
        send_mail(
            "Subject Sample Mail",
            "Here is the message.",
            "o.kazuhide.takahashi+from@gmail.com",
            ["o.kazuhide.takahashi+to@gmail.com"],
            fail_silently=False,
        )
        return HttpResponse('sample mail sended: success')
    except Exception as e:
        return HttpResponse('sample mail error: ' + str(e))


def bootstrap5(request):
    logger = logging.getLogger('django')
    logger.info('bootstrap5')
    return render(request, "sample/bootstrap5.html", {})

def adminlte3(request):
    logger = logging.getLogger('django')
    logger.info('adminlte3')
    return render(request, "sample/adminlte3.html", {})

