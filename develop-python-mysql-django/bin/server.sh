#!/usr/bin/env bash

WORKDIR=/home/ec2-user
LOGIN_USER="ec2-user"
while getopts r OPT
do
  case $OPT in
     r) LOGIN_USER="root"; WORKDIR=/root;;
#     *) echo "該当なし（OPT=$OPT）";;
  esac
done

docker compose exec -u ${LOGIN_USER} -w ${WORKDIR} develop-python-mysql-django-serverside /bin/zsh
