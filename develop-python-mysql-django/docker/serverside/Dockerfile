FROM amazonlinux:2023

WORKDIR /home/ec2-user/

RUN dnf install -y systemd kernel-libbpf shadow-utils util-linux util-linux-user cronie \
    gcc glibc zlib-devel \
    readline readline-devel libffi-devel \
    zip unzip tar gzip xz bzip2 bzip2-devel git zsh sed sudo procps-ng \
    sqlite sqlite-devel openssl openssl-devel \
    vi vim nano \
    nginx \
    && systemctl enable crond.service \
    && systemctl enable nginx.service

# mysql client
RUN dnf -y localinstall https://dev.mysql.com/get/mysql80-community-release-el9-1.noarch.rpm
RUN dnf -y install mysql mysql-community-client mysql-devel

RUN echo "Create ec2-user" && \
    useradd "ec2-user" && \
    echo "ec2-user ALL=NOPASSWD: ALL" >> /etc/sudoers && \
    chown ec2-user:ec2-user /home/ec2-user

#RUN echo "# Created by newuser for 5.8.1" > /home/ec2-user/.zshrc && chown ec2-user:ec2-user /home/ec2-user/.zshrc
#RUN echo "# Created by newuser for 5.8.1" > /home/ec2-user/.zshrc && chown ec2-user:ec2-user /home/ec2-user/.zshrc

#RUN chown ec2-user:nginx /var/www/html

ENV USER ec2-user
ENV HOME /home/$USER

USER $USER
WORKDIR $HOME

#RUN chsh -s /usr/bin/zsh

RUN git clone --recursive https://github.com/sorin-ionescu/prezto.git $HOME/.zprezto
#RUN rm /home/ec2-user/.zshrc
RUN ln -s $HOME/.zprezto/runcoms/zlogin $HOME/.zlogin \
    && ln -s $HOME/.zprezto/runcoms/zlogout   $HOME/.zlogout \
    && ln -s $HOME/.zprezto/runcoms/zpreztorc $HOME/.zpreztorc \
    && ln -s $HOME/.zprezto/runcoms/zprofile  $HOME/.zprofile \
    && ln -s $HOME/.zprezto/runcoms/zshenv    $HOME/.zshenv \
    && ln -s $HOME/.zprezto/runcoms/zshrc     $HOME/.zshrc

#RUN echo "export LANG=ja_JP.UTF-8" >> $HOME/.zshrc

#RUN git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto" && \
#    setopt EXTENDED_GLOB && \
#    for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do \
#      ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"; \
#    done

SHELL ["/usr/bin/zsh", "-c"]

#RUN . $HOME/.zshrc

ENV PYENV $HOME/.pyenv
ENV PATH $PYENV/bin:$PATH
ENV PATH $PYENV/shims:${PATH}

RUN echo "Install pyenv and python" && \
    git clone https://github.com/pyenv/pyenv.git ~/.pyenv && \
    echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc && \
    echo '[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc && \
    echo 'eval "$(pyenv init -)"' >> ~/.zshrc && \
    . ~/.zshrc && \
    pyenv --version && \
    pyenv install 3.12.1 && \
    pyenv global 3.12.1 && \
    pyenv rehash && \
    python --version

USER $USER
WORKDIR $HOME

#RUN zsh
# ~/.pyenv/shims/pip
RUN yes | pip install --upgrade pip && pip --version
RUN yes | pip install wheel
RUN yes | pip install django && python -c "import django; print(django.get_version())"
# reqired mysql-devel
#   https://www.den-tsu.net/mysqlclient/
RUN yes | pip install mysqlclient

RUN yes | pip install setuptools
RUN yes | pip install ppretty
RUN yes | pip install django-bootstrap5
RUN yes | pip install django-adminlte-3

#RUN yes | pip install uwsgi && \
#    sudo ln -s /usr/local/pyenv/shims/uwsgi /usr/bin/uwsgi && \
#    sudo mkdir -p /var/run/uwsgi && \
#    sudo chown nginx:nginx /var/run/uwsgi && \
#    sudo chmod +w /var/run/uwsgi

# copy files
# COPY cron.d/* /etc/cron.d

WORKDIR /var/www/html

USER root
WORKDIR /root

EXPOSE 8080 8443

# init
CMD ["/usr/sbin/init"]

# after

# cd /var/www/html
# django-admin startproject developPythonMysqlDjango .
# python manage.py migrate
# python manage.py runserver 0.0.0.0:8080
