#!/usr/bin/env bash

# OS Setting
timedatectl set-timezone Asia/Tokyo
localectl set-locale LANG=ja_JP.UTF-8

systemctl stop php-fpm
systemctl start php-fpm
systemctl status php-fpm
systemctl stop nginx
systemctl start nginx
systemctl status nginx

