
create database `orbit-sales` CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
-- create database `orbit-sales-test` CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
-- create database `orbit-sales-dusk` CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;

-- create database `orbit-sales-metabase` CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;

-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;

-- flush privileges;
