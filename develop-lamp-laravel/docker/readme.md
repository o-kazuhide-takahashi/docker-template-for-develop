# 開発 Python/MySQL/Django

---

## Usage

start container

```shell
docker compose up -d
```

run develop server

```shell
bin/server.sh
cd /var/www/html
python manage.py runserver --settings=samplesite.settings 0.0.0.0:8080
```

stop container

```shell
docker compose down
```

### login root user

```shell
bin/server.sh -r
```

## Docker Image Update

DockerFileを変更した場合はキャッシュ無しビルドで更新すること

```shell
docker compose ps
docker compose down

docker compose build --no-cache

docker compose up -d
```

## 参考資料

### 【Docker】Oracleを無料で簡単にローカルに構築する

https://zenn.dev/re24_1986/articles/29430f2f8b4b46

### Amazon Linux 2023のDockerイメージを使う

https://qiita.com/charon/items/b54dac9c3a72738a18e4

### systemd, crond and Docker Container

https://dev.to/mandraketech/systemd-crond-and-docker-container-1mob

### AWSのEC2デフォルトシェルをbashからzshに変更する

https://zenn.dev/katsun0921/articles/idea_aws_ec2_01

### django+nginx+uwsgiの環境構築

https://qiita.com/m_kiita/items/aa53d31d7bbfc9566edd

### Python3.12の新機能 (まとめ)

https://qiita.com/ksato9700/items/2eac0d2bd0d0dc125f13
